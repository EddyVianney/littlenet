import React from'react'
import VideoListItem from '../components/video-list-item'

const VideoList = function(props){
    
    const movieList = props.movieList;
    return (
        <div>   
             <ul>
                {movieList.map(movie => {
                        return <VideoListItem key={movie.id} movie={movie} callback={receiveMovie}/> 
                    })}
            </ul>   
        </div>
 

    )

    function receiveMovie(movie)
    {
        props.callback(movie);
    }
}

export default VideoList ;

