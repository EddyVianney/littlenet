import React,{Component} from'react'
import SearchBar from '../components/search-bar'
import VideoList  from './video-list'
import VideoDetail from '../components/video-detail'
import Video from '../components/video'
import axios from 'axios'


const API_END_POINT = "https://api.themoviedb.org/3/"
const POPULAR_MOVIES_URL = "discover/movie?language=fr&sort_by=popularity.desc&include_adult=false&append_to_response=images"
const API_KEY = "api_key=e02ad35db3594893c43f455903fef19b"
const BASE_URL = "https://www.youtube.com/embed/"
const SEARCH_URL ="search/movie?language=fr&include_adult=false"



class App extends Component{

    constructor(props) {    

        super(props);
        this.state = {movieList:{},currentMovie:{}}
    }

    componentWillMount()
    {
        this.initMovies();
    } 
	

    initMovies(){

        axios.get(`${API_END_POINT }${POPULAR_MOVIES_URL}&${API_KEY}`).then(function(response)
        {
            this.setState({movieList:response.data.results.slice(1,7),currentMovie:response.data.results[0]},function(){
                this.uploadCurrentMovie();
            });
        }.bind(this));
    }

    uploadCurrentMovie()
    {

        axios.get(`${API_END_POINT}movie/${this.state.currentMovie.id}?${API_KEY}&append_to_response=videos&include_adult=false`).then(function(response)
        {
             const youtubeKey = response.data.videos.results[0].key;
             let newCurrentMovieState = this.state.currentMovie;
             newCurrentMovieState.videoId  = youtubeKey;
             this.setState({currentMovie:newCurrentMovieState})
             console.log(newCurrentMovieState);

        }.bind(this));
    }
   
    receiveMovie2(movie)
    {
        this.setState({currentMovie:movie},function(){
            this.uploadCurrentMovie();
            this.recommandedMovies();
        });
    }
    receiveInputField(text)
    {
        axios.get(`${API_END_POINT }${SEARCH_URL}&${API_KEY}&query=${text}`).then(function(response)
        {
            if(response.data && response.data.results[0])
            {
                if(response.data.results[0].id != this.state.currentMovie.id)
                {
                    this.setState({currentMovie:response.data.results[0]},function(){
                        this.uploadCurrentMovie();
                        this.recommandedMovies();
                    });
                }
            }
        }.bind(this));  
    }

    recommandedMovies()
    {
        axios.get(`${API_END_POINT }movie/${this.state.currentMovie.id}/recommendations?&${API_KEY}&language=fr`).then(function(response)
        {
            this.setState({movieList:response.data.results.slice(0,7)});
        }.bind(this));
    }
    render()
    {   
        
        const movieListNotEmpty = function(){
            if(this.state.movieList.length>= 5)
            {
                return  <VideoList movieList={this.state.movieList} callback={this.receiveMovie2.bind(this)}/>
            }
        }.bind(this)

        return(
            <div>
                <div className="searchbar">
                    <SearchBar receiveSearch ={this.receiveInputField.bind(this)}/>
                </div>
                <div className="row">
                    <div className="col-md-8">
                        <Video  videoId = {this.state.currentMovie.videoId}/>
                        <VideoDetail title = {this.state.currentMovie.title} description = {this.state.currentMovie.overview}/>
                    </div>
                    <div className="col-md-4">
                      {movieListNotEmpty()}   
                    </div>
                </div>
            </div>

        )    
    }
}

export default App ;

