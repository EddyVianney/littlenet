import React from'react'

const IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500";
const VideoListItem = function(props)
{
    const movie = props.movie;
    return <li className="list-group-item" onClick = {handleOnclick}>
                <div className="media">
                    <div className="media-left">
                         <img className="media-object img-rounded" height="100Px" width="100px" src={`${IMAGE_BASE_URL}${movie.poster_path}`} />
                    </div>
                    <div className="media-body">
                         <h4 className="title-item">{movie.title}</h4>
                      </div>
                </div>
          </li>

    function handleOnclick(){
        props.callback(movie);
    }
}



export default VideoListItem ;