import React,{Component} from  'react'

class SearchBar extends React.Component{
    constructor(props)
    {
        super(props);
        this.state = {searchText: "",placeholder:"Cherchez un film ..."}
    }
    render(){
        return(
            <div className="row">
                <div className="col-md-8 input-group">
                    <input type="text" className="form-control  input-lg" onKeyUp = {this.handleChange.bind(this)} placeholder={this.state.placeholder}/>
                    <span  className="input-group-btn">
                        <button className="btn btn-secondary" onClick={this.handleOnclick.bind(this)}><span className="glyphicon glyphicon-search"></span>GO</button>
                    </span>
                </div>
            </div>
        ) 
    }

    handleChange(event)
    {
        this.setState({searchText:event.target.value});
    }
    handleOnclick(event)
    {
        this.props.receiveSearch(this.state.searchText);
    }
}

export default SearchBar;