import React from'react'

const VideoDetail = function({title,description}){

    return (
        <div>
            <h1 className="video-title">{title}</h1>
            <p className="video-desc">{description}</p>
        </div>
    )
    
}

export default VideoDetail ;
