

Lancer le projet:
	taper npm install ( � la racine du r�pertoire qui contient le projet)
	taper npm start dans le meme terminal
	taper localhost:8080 dans le navigateur

Langages utilis�es:
		 Boostrap, React, AJAX


Organisation du mini-projet:
	Le dossier src contient les dossiers components et  containers.
	Le dossier components contient les composants de l'application.
	Le dossier containers contient tous les conteneurs
	Le dossier style contient la feuille de style css
	Le dossier node-module s'installe facilement sur un terminal (linux ou windows) � l'aide de la commande npm install.


Les components:
	Le composant video-list-item: il g�re les les g�re les vid�os recommend�es. Il communique avec le composant video.
	Le composant video: il permet de recup�rer une vid�o sur youtube.
	Le composant video-detail: il recup�re les informations  de la video tels que le titre et la description.
	Le composant search-bar:  il recup�re  le nom d'un film  et l'envoie � l'application.
	
Il ya deux composants:
	L'application elle-meme qui est le conteneur g�n�ral. le container video-list contient.
	la liste des videos recommend�es.


Les donn�es:
Les donn�es sont fournies par l'API themoviedb.  Les requetes sont effectu�es en AJAX.


Les connaissances acquises:
	cr�ation d'un composant  react
	gestion de l'asynchronisme dans les requetes AJAX (bien s'assurer que l'on a les donn�es demand�es avant de vouloir les afficher)
	communication entre les composants:
	La Communication d'un composant enfant vers un composant parent se fait � l'aide des callbacks.
        Par exemple, le composant video-list-item communique avec le composant vid�o de lamani�re suivante:
	Il envoie un film au conteneur video-list. Le conteneur envoie � son tour la video � l'application.
	L'application transmet enfin la video au composant video.